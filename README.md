# Docker Wrapper to enable internet to containers when docker0 is blocked and --net host is ok.

This project provides a wrapper for the Docker command-line tool that automatically deploys a proxy server to allow containers to access the internet when the docker0 interface is blocked. This is useful for cases where the docker0 interface is blocked or not available, preventing containers from accessing the internet (and when you don't have access to the host firewall).


## Latest build

You can download the [latest version of the dkr for linux ](https://gitlab.com/api/v4/projects/44679023/jobs/artifacts/main/raw/dist/dkr?job=build) command from GitLab's artifact repository by using the following command:

```shell
mkdir ~/bin
cd ~/bin
curl -L https://gitlab.com/api/v4/projects/44679023/jobs/artifacts/main/raw/dist/dkr?job=build --output dkr
chmod +x dkr
```
This will download the latest version of the dkr command and add it to your ~/bin directory. You can then add the ~/bin directory to your PATH so that you can use the dkr command from anywhere. Or you can add it to the path in each terminal :

```shell
export PATH=$PATH:~/bin
```

Alternatively, you can add the export command to your bashrc file to make the dkr command available every time you start a new terminal session.


## Usage

The wrapper script provides the same interface as the Docker command-line tool. It automatically starts a proxy if necessary, adds the information to each container run, and adds the right network during a build. To use the wrapper, simply replace docker with dkr in your commands. For example:

```shell
$ dkr run alpine echo "Hello, world!"
```

This will run the alpine container and execute the echo "Hello, world!" command inside the container, with proxy settings automatically applied.


## How it works : Proxy configuration

The proxy server used by the wrapper is a Squid proxy server running in a docker container (on network `--net host`). The proxy settings are automatically added to the http_proxy and https_proxy environment variables for each container launched with the wrapper.

The reason for using a proxy server is that certain configurations of machines do not allow communication towards the docker0 interface. This makes the containers unable to access the internet. The solution used in this wrapper is to deploy a container that acts as a proxy on the --network host, port 3128. This allows other Docker containers to access the network by automatically configuring the proxy in each container when the run command is called.
