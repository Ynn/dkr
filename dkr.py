import os
import sys
import subprocess
import getpass
from typing import List
import grp
import json


PROXY_NAME: str = "dkr_proxy_server"


def check_proxy_status(need_sudo) -> bool:
    """
    Check if the Docker proxy server is running.
    If running, return True.
    If not running, delete the container and its volume if it exists and return False.
    """
    try:
        docker_command = ["docker", "ps", "--filter", f"name={PROXY_NAME}", "--format", "{{.Status}}"]
        if need_sudo:
            docker_command.insert(0, 'sudo')

        output = subprocess.check_output(docker_command,stderr=subprocess.DEVNULL)

        if "Up" in output.decode("utf-8"):
            return True
        else:
            # Delete the container and its volume if it exists
            try:
                docker_command = ["docker", "rm", "-v", PROXY_NAME]
                if need_sudo:
                    docker_command.insert(0, 'sudo')
                subprocess.check_call(docker_command,stderr=subprocess.DEVNULL)

                print("Container has been removed.")
            except subprocess.CalledProcessError as e:
                if "is already in progress" in str(e):
                    print("Container removal is already in progress.")
                else:
                    print(f"Unable to remove container: {e}")
            return False
    except subprocess.CalledProcessError:
        pass
    return False


def start_proxy_server(need_sudo) -> None:
    """
    Start the Docker proxy server.
    """
    docker_command = ["docker", "run", "-d", "--name", PROXY_NAME, "--network", "host", "ubuntu/squid"]
    if need_sudo:
        docker_command.insert(0, 'sudo')
    subprocess.check_call(docker_command, stdout=subprocess.DEVNULL) # , "-e", "http_access=allow all", ?
    print("Proxy server started.")


def docker_run(need_sudo, args: List[str]) -> None:
    """
    Wrapper function for the "docker run" command.
    """
    # Check if the Docker proxy server is running and start it if necessary
    if not check_proxy_status(need_sudo):
        print("The proxy server is not running. Starting the proxy server...")
        start_proxy_server(need_sudo)

    # Vérifier si l'argument --network est présent
    network_index = args.index('--net') if '--net' in args else -1
    if network_index == -1:
        network_index = args.index('--network') if '--network' in args else -1

    if network_index != -1:
        # Récupérer le nom du réseau
        network_name = args[network_index + 1]

        # Récupérer les noms d'hôte des conteneurs sur le même réseau
        docker_command = ['docker', 'network', 'inspect', network_name]
        if need_sudo:
            docker_command.insert(0, 'sudo')
        output = subprocess.check_output(docker_command)
        network_data = json.loads(output.decode('utf-8'))
        container_names = [container['Name'][:] for network in network_data for container in network['Containers'].values()]

        # Ajouter les noms d'hôte à la variable d'environnement "no_proxy"
        if container_names:
            no_proxy = os.environ.get('no_proxy', '')
            if no_proxy:
                no_proxy += ","
            for container_name in container_names:
                no_proxy += container_name + ","
            no_proxy = no_proxy[:-1]

    # Construire la commande Docker
    docker_command = [
        "docker",
        "run",
        "--add-host", "host.docker.internal:host-gateway",
        "-e","http_proxy=http://host.docker.internal:3128",
        "-e","https_proxy=http://host.docker.internal:3128",
    ]
    if network_index != -1 and container_names:
        docker_command += ["-e", f"no_proxy={no_proxy}"]
    
    if need_sudo:
        docker_command.insert(0, 'sudo')

    docker_command += args
    print(" ".join(docker_command))
    subprocess.run(docker_command, env=os.environ)


def docker_build(need_sudo, args: List[str]) -> None:
    """
    Wrapper function for the "docker build" command.
    """
    docker_command = ["docker", "build", "--network", "host"]
    if need_sudo:
        docker_command.insert(0, 'sudo')
    docker_command += args
    print(" ".join(docker_command))
    subprocess.run(docker_command, env=os.environ)


def docker_cmd(need_sudo, *args: str) -> None:
    """
    Wrapper function for the "docker" command.
    """
    if args[0] == "run":
        docker_run(need_sudo,args[1:])
    elif args[0] == "build":
        docker_build(need_sudo,args[1:])
    else:
        docker_command = ["sudo", "docker"] + list(args)
        subprocess.run(docker_command, env=os.environ)

def check_sudo() -> bool:
    # Check if user has permission to run Docker without sudo
    if grp.getgrnam('docker').gr_mem.__contains__(getpass.getuser()):
        return False

    try:
        # Update the password timeout for sudo
        subprocess.run(["sudo", "-v"], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        # Prompt the user for their sudo password
        sudo_password = getpass.getpass("Please enter your sudo password: ")

        # Update the password timeout for sudo using the provided password
        sudo_command = ["sudo", "-v"]
        subprocess.run(sudo_command, input=sudo_password.encode('utf-8'), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    finally:
        return True

if __name__ == "__main__" :
    if len(sys.argv) == 1 :
        sys.argv += ["help"]

    need_sudo:bool= check_sudo()

    # Appel du wrapper de commande "docker"
    docker_cmd(need_sudo,*sys.argv[1:])